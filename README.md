<h1 align="center">Step-project-forkio</h1>


---
- [List of Technologies Used](#List-of-Technologies-Used)
  - [Clone](#Clone)
  - [How to run local](#How-to-run-local)
- [Contributing](#contributing)
  - [git flow](#git-flow)
  - [issue flow](#git-flow)
  - [Team](#team)
  - [Tasks Completed by Each Member](#Tasks-Completed-by-Each-Member)
- [FAQ](#faq)

---

## List of Technologies Used

- html
- css
- JS
- bootstrap
- API server (fetch)
- ES6 modules



### Clone

- Clone this repo to your local machine using `https://gitlab.com/savenko0097/step-card`


### How to run local

You can use a live server in VS Code to run local this project.

## Contributing

---

Before sending any pull request, please discuss requirements/changes to be implemented using an existing issue or by creating a new one. All pull requests should be done into `main` branch.


---

### Git flow

We have **main** and **others** branches.  
All **feature** branches must be merged into [main] (https://gitlab.com/savenko0097/step-card) branch!!!


#### Step 1

- **Option 1**

  - 👯 Clone this repo to your local machine using `https://gitlab.com/savenko0097/step-card`

- **Option 2**

  - create new branch from development branch

  #### Step 2

- add some code to your new branch

#### Step 3

- Run git add . in the terminal. This will track any changes made to the folder on your system, since the last commit. As this is the first time you are committing the contents of the folder, it will add everything

#### Step 4

- Run git commit -m"insert Message here". This will prepare the added/tracked changes to the folder on your system for pushing to Github. Here, insert Message here can be replaced with any relevant commit message of your choice.

#### Step 5

- Run git push origin “branch_name”. Note that the last word in the command “branch_name”, is not a fixed entry when running git push. It can be replaced with any relevant “branch_name”.

#### Step 6

- 🔃 Create a new pull request.



---

### Issue flow

#### Step 1

-go to [!issues](https://gitlab.com/savenko0097/step-card/-/issues) and click `New issue` button

#### Step 2

when creating [issue](https://gitlab.com/savenko0097/step-card/-/issues/new) you should add name of the issue, description, choose assignee, label, project. If issue is a `User Story` you should link it with corresponding tasks, and corresponding tasks should be linked to issue.

#### Step 3

if issue is in work it should be placed in proper column on dashboard according to its status.

---

## Team

### Development team
[<img src="https://gitlab.com/uploads/-/system/user/avatar/13362400/avatar.png" width="150">](https://gitlab.com/grishanych) <span>Grygorii Nych</span>
<br>
<br>
[<img src="https://avatars.githubusercontent.com/u/115550388?v=4" width="150">](https://gitlab.com/savenko0097) <span>Anastasiia Savenko</span>
<br>
[<img src="https://secure.gravatar.com/avatar/3fed8d42c384d4bc839652c462e54b87?s=800&d=identicon" width="150">](https://gitlab.com/tatko8) <span>Serhii Ba</span>

## Tasks Completed by Each Member

1. Grygorii Nych:
   - Сreated the ability to logout.
   - Created function to edit card using PUT method.
   - Split the code into ES6 modules

2. Anastasiia Savenko:
   - Created html structure using bootstrap.
   - Сreated the ability to login.
   - Created function to add card and post data to API server.
   - Сreated the ability to store data in localstorage.
   - Created function to delete card using DELETE method.


## FAQ

- **How do I do _specifically_ so and so?**
  - No problem! Just do this.