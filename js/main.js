'use strict';

import {
    URL,
    EMPTY_section,
    FORM_sectinon,
    CARD_section,
    cardInfo,
    RECORD_CARD_section, LOGIN_btn, REGISTRATION_btn,
    LOG_OUT_btn, CLOSE_MODAL_btn,
    CREATE_CARD_btn,
    SAVE_CHANGES_CARD_btn,
    ADD_CARD_btn,
    MODAL_card,
    closeModal,
    FILTER_btn,
    USER_email,
    USER_password,
    USER_repeat_pass,
    FILTER,
    DOCTOR_btn,
    URGENCY_btn,
    USER_purposeVisit,
    USER_brief,
    USER_urgancyReg,
    USER_urgancyPr,
    USER_urgancyUrg,
    USER_fullName,
    DENTIS_input,
    CARDIOLOGIST_input,
    THERAPIST_input,
    USER_dentistDate,
    USER_cardioPressure,
    USER_cardioMass,
    USER_cardioDiseases,
    USER_cardioAge,
    USER_therapistAge

} from './constants.js'

import { Visit, VisitDentist, VisitCardiologist, VisitTherapist } from './classes.js'


//USER CARD dashboard


LOG_OUT_btn.style.display = 'none';
SAVE_CHANGES_CARD_btn.style.display = 'none';

//REGISTRATION----------------------------------------------------------------------------REGISTRATION//
REGISTRATION_btn.addEventListener('click', function (event) {
    event.preventDefault();

    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/; // check correct email
    const latinPattern = /^[A-Za-z0-9]+$/; // check latin for email

    if (
        USER_password.value === USER_repeat_pass.value &&
        emailPattern.test(USER_email.value) &&
        latinPattern.test(USER_password.value) &&
        latinPattern.test(USER_email.value.split('@')[0])
    ) {
        // save data to localStorage
        localStorage.setItem('loggedIn', 'true');
        localStorage.setItem('login', USER_email.value);
        localStorage.setItem('password', USER_password.value);

        // add hash
        window.location.hash = 'card-section';

        // 
        CARD_section.style.display = 'block';
        CARD_section.append(RECORD_CARD_section);
        FORM_sectinon.style.display = 'none';
        LOGIN_btn.style.display = 'none';
        LOG_OUT_btn.style.display = 'block';
    } else {
        alert('Uncorrect email or password');
    }
});


CLOSE_MODAL_btn.addEventListener('click', () => {
    FORM_sectinon.style.display = 'none';
});

closeModal.addEventListener('click', () => {
    FORM_sectinon.style.display = 'none';
});

document.addEventListener('keydown', function (e) {
    if (e.key === 'Escape') {
        FORM_sectinon.style.display = 'none';
    }
});

document.addEventListener('click', function (event) {
    if (event.target === FORM_sectinon) {
        FORM_sectinon.style.display = 'none';
    }
});

//REGISTRATION--------------------------------------END--------------------------------------REGISTRATION//

// ----------------------------------------------------logout button
LOG_OUT_btn.addEventListener('click', () => {
    // remove data from localStorage
    localStorage.removeItem('loggedIn');
    localStorage.removeItem('login');
    localStorage.removeItem('password');
    localStorage.removeItem('visit');

    // remove hash
    window.location.hash = '';
    EMPTY_section.style.display = 'block'; // section with "No items have been added...."
    FORM_sectinon.style.display = 'none'; // 'blue' section
    LOG_OUT_btn.style.display = 'none';
    LOGIN_btn.style.display = 'block';
});
// ---------------------END-------------------------------logout button



window.addEventListener('DOMContentLoaded', () => {
    const loggedIn = localStorage.getItem('loggedIn');
    const visitData = localStorage.getItem('visit');

    if (loggedIn === 'true' && visitData) {
        CARD_section.style.display = 'block';
        CARD_section.append(RECORD_CARD_section);
        FORM_sectinon.style.display = 'none';
        LOGIN_btn.style.display = 'none';
        LOG_OUT_btn.style.display = 'block';

        if (visitData) {
            visit = JSON.parse(visitData);
            visit.forEach((card) => {
                loadCardsFromLocalStorage(card);
                createRecord(card)
            });
        }
    } else {
        CARD_section.style.display = 'none';
        LOGIN_btn.style.display = 'block';
    }
});



// ========================= EVENTS ============================================================ //

//show input form
LOGIN_btn.addEventListener('click', () => {
    EMPTY_section.style.display = 'none'; // section with "No items have been added...."
    FORM_sectinon.style.display = 'block'; // 'blue' section
});



// // show modal to create new card
ADD_CARD_btn.addEventListener('click', () => {
    MODAL_card.show();
    CREATE_CARD_btn.style.display = 'block';
    SAVE_CHANGES_CARD_btn.style.display = 'none';

});


// ----------------------------------------add input when you choose doctor
DOCTOR_btn.addEventListener('change', () => {
    const selectedDoctor = DOCTOR_btn.value;
    if (selectedDoctor === 'Dentist') {
        DENTIS_input.style.display = 'block';
        CARDIOLOGIST_input.style.display = 'none';
        THERAPIST_input.style.display = 'none';
    } else if (selectedDoctor === 'Cardiologist') {
        CARDIOLOGIST_input.style.display = 'block';
        DENTIS_input.style.display = 'none';
        THERAPIST_input.style.display = 'none';
    } else if (selectedDoctor === 'Therapist') {
        THERAPIST_input.style.display = 'block';
        CARDIOLOGIST_input.style.display = 'none';
        DENTIS_input.style.display = 'none';
    } else {
        THERAPIST_input.style.display = 'none';
        CARDIOLOGIST_input.style.display = 'none';
        DENTIS_input.style.display = 'none';
    }
});

// ----------------------------------------END input when you choose doctor



//----------------------------------------COOKIES and save login\password
let userID = 0;

class RegisteredUsers {
    constructor(email, password) {
        this.email = email;
        this.password = password;
        this.createID();
    }

    createID() {
        userID++;
        this.userID = userID;
    }
}

//----------------------------------------END---------COOKIES and save login\password


// const TOKEN = 'd34178da-459e-44e0-8245-6c11a907892b';

const TOKEN = 'cfb04bf6-42a2-4a38-a042-063049e8b98c'

let visit = JSON.parse(localStorage.getItem('visit')) || [];


//------------------------START ----------------function createRecord

function createRecord(response) {

    try {
        let standartVisitData = new Visit(DOCTOR_btn.value, USER_purposeVisit.value, USER_brief.value, URGENCY_btn.value, USER_fullName.value);
        const recordCard = document.getElementById('record-card');
        if (!recordCard) {
            EMPTY_section.style.display = 'block';
        }

        let cardBody = document.createElement('div');
        cardBody.classList.add('card-body-after-save-card', "p-2", 'm-2');
        CARD_section.classList.add('card-section')
        CARD_section.append(recordCard)
        recordCard.prepend(cardBody);

        let recordTitle = document.createElement('h4');
        recordTitle.classList.add('card-title', 'text-primary');
        recordTitle.textContent = 'Welcome, ' + standartVisitData.userName;
        cardBody.append(recordTitle);

        let recordDoctor = document.createElement('h5');
        recordDoctor.classList.add('card-title');
        recordDoctor.textContent = 'Doctor: ' + standartVisitData.doctor;
        cardBody.append(recordDoctor);

        let showMoreBtn = document.createElement('button');
        showMoreBtn.classList.add('btn', 'btn-outline-primary');
        showMoreBtn.type = 'button';
        showMoreBtn.textContent = 'SHOW MORE';
        cardBody.append(showMoreBtn);

        let cardsActionBtn = document.createElement('div');
        cardsActionBtn.classList.add('m-2', 'd-md-flex', 'justify-content-md-between')
        cardBody.prepend(cardsActionBtn);

        let editBtn = document.createElement('button');
        editBtn.classList.add('btn', 'btn-outline-primary');
        editBtn.type = 'button';
        editBtn.textContent = 'edit';
        cardsActionBtn.prepend(editBtn);


        editBtn.addEventListener('click', () => {
            console.log(777)
            // Set up the card form with existing data for editing
            USER_fullName.value = standartVisitData.userName;
            DOCTOR_btn.value = standartVisitData.doctor;
            USER_purposeVisit.value = standartVisitData.purpose;
            USER_brief.value = standartVisitData.brief;
            URGENCY_btn.value = standartVisitData.urgency;
            USER_dentistDate.value = standartVisitData.date; // Replace this with the appropriate value for the Dentist's last visit date, if available
            USER_cardioAge.value = ''; // Replace this with the appropriate value for the Cardio Age, if available
            USER_cardioPressure.value = ''; // Replace this with the appropriate value for the Cardio Pressure, if available
            USER_cardioMass.value = ''; // Replace this with the appropriate value for the Cardio Mass Index, if available
            USER_cardioDiseases.value = ''; // Replace this with the appropriate value for the Cardio Diseases, if available

            MODAL_card.show(); // Show the modal for editing the card

            SAVE_CHANGES_CARD_btn.style.display = 'block';
            CREATE_CARD_btn.style.display = 'none';


            // Add an event listener to the "Save Changes" button inside the modal for updating the card information
            SAVE_CHANGES_CARD_btn.addEventListener('click', () => {
                // Fetch the PUT request to update the card information on the server
                const urlPut = `${URL}/${response.id}`;
                fetch(urlPut, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${TOKEN}`
                    },
                    body: JSON.stringify({
                        id: response.id,
                        title: `Visit to ${DOCTOR_btn.value}`,
                        userName: USER_fullName.value,
                        doctor: DOCTOR_btn.value,
                        purpose: USER_purposeVisit.value,
                        brief: USER_brief.value,
                        urgency: URGENCY_btn.value,
                        visitDate: USER_dentistDate.value,
                        age: USER_cardioAge.value,
                        pressure: USER_cardioPressure.value,
                        massIndex: USER_cardioMass.value,
                        diseases: USER_cardioDiseases.value,
                    })
                })
                    .then(response => response.json())
                    .then(updatedData => {
                        // Update the card content on the screen with the edited information
                        recordTitle.textContent = 'Welcome, ' + updatedData.userName;
                        recordDoctor.textContent = 'Doctor: ' + updatedData.doctor;
                        recordPurpose.textContent = 'Purpose: ' + updatedData.purpose;
                        recordBrief.textContent = 'Client brief: ' + updatedData.brief;
                        recordUrgancy.textContent = 'Urgancy: ' + updatedData.urgency;
                        recordAge.age = 'Age: ' + updatedData.age;
                        // recordPurpose.textContent = 'Purpose: ' + updatedData.age;


                        // Update the showMoreSection content here if needed for Dentist, Cardio, and Therapist

                        // Close the modal after saving changes
                        MODAL_card.hide();
                    })
                    .catch(error => {
                        // Handle errors if the update fails
                        console.error('Error updating card data:', error);
                    });
            });
        });

//---------------------------------------------------------
        let deleteBtn = document.createElement('button');
        deleteBtn.classList.add('btn', 'btn-outline-primary');
        deleteBtn.type = 'button';
        deleteBtn.textContent = 'delete';
        cardsActionBtn.append(deleteBtn);

        deleteBtn.addEventListener('click', async () => {
            try {
                const data = await fetch(`https://ajax.test-danit.com/api/v2/cards/${response.id}`, {
                    method: 'DELETE',
                    headers: {
                        'Authorization': `Bearer ${TOKEN}`
                    },
                });
                if (data.ok) {
                    cardBody.remove();
                    removeFromLocalStorage(response.id);
                } else {
                    console.error('cant delete');
                }
            } catch (error) {
                console.error('error:', error);
            }
        });




        showMoreBtn.addEventListener('click', () => {
            showMoreSection.style.display = 'block';
            showMoreBtn.style.display = 'none'
        })



        let showMoreSection = document.createElement('div');
        cardBody.append(showMoreSection);
        showMoreSection.style.display = 'none';


        let recordPurpose = document.createElement('p');
        recordPurpose.classList.add('card-text');
        recordPurpose.textContent = 'Purpose: ' + standartVisitData.purpose;
        showMoreSection.append(recordPurpose);

        let recordBrief = document.createElement('p');
        recordBrief.classList.add('card-text', 'card-brief');
        recordBrief.textContent = 'Client brief: ' + standartVisitData.brief;
        showMoreSection.append(recordBrief);

        let recordUrgancy = document.createElement('p');
        recordUrgancy.classList.add('card-text');
        recordUrgancy.textContent = 'Urgancy: ' + standartVisitData.urgency;
        if (URGENCY_btn.value === 'Regular') {
            recordUrgancy.classList.add('text-success')
        } else if (URGENCY_btn.value === 'Priority') {
            recordUrgancy.classList.add('text-warning')
        } else if (URGENCY_btn.value === 'Urgent') {
            recordUrgancy.classList.add('text-danger')
        }

        let recordAge = document.createElement('p');
        recordAge.classList.add('card-text');
        recordAge.textContent = 'Last date: ' + standartVisitData.age;


        showMoreSection.append(recordUrgancy);

        if (DOCTOR_btn.value === 'Cardiologist') {
            let cardioCardData = new VisitCardiologist(standartVisitData.doctor,
                standartVisitData.purpose,
                standartVisitData.brief,
                standartVisitData.urgency,
                standartVisitData.userName,
                USER_cardioPressure.value,
                USER_cardioMass.value,
                USER_cardioDiseases.value,
                USER_cardioAge.value);

            let showMoreCardio = document.createElement('div');
            showMoreSection.append(showMoreCardio);


            let cardioPressure = document.createElement('p');
            cardioPressure.classList.add('card-text');
            cardioPressure.textContent = 'Pressure: ' + cardioCardData.pressure;
            showMoreCardio.append(cardioPressure);

            let cardioMassIndex = document.createElement('p');
            cardioMassIndex.classList.add('card-text');
            cardioMassIndex.textContent = 'MassIndex: ' + cardioCardData.massIndex;
            showMoreCardio.append(cardioMassIndex);


            let cardioDiseases = document.createElement('p');
            cardioDiseases.classList.add('card-text');
            cardioDiseases.textContent = 'Diseases: ' + cardioCardData.diseases;
            showMoreCardio.append(cardioDiseases);

            let cardioAge = document.createElement('p');
            cardioAge.classList.add('card-text');
            cardioAge.textContent = 'Age: ' + cardioCardData.age;
            showMoreCardio.append(cardioAge);


        } else if (DOCTOR_btn.value === 'Dentist') {

            let dentistCardData = new VisitDentist(standartVisitData.doctor, standartVisitData.purpose, standartVisitData.brief, standartVisitData.urgency, standartVisitData.userName, USER_dentistDate.value);

            let showMoreDentist = document.createElement('div');
            showMoreSection.append(showMoreDentist);

            let dentistVisit = document.createElement('p');
            dentistVisit.classList.add('card-text');
            dentistVisit.textContent = 'Last visit:' + dentistCardData.lastVisit;
            console.log(dentistVisit);
            showMoreDentist.append(dentistVisit);

            console.log(dentistCardData);


        } else if (DOCTOR_btn.value === 'Therapist') {

            let therapistCardData = new VisitTherapist(standartVisitData.doctor,
                standartVisitData.purpose,
                standartVisitData.brief,
                standartVisitData.urgency,
                standartVisitData.userName,
                USER_therapistAge.value)


            let showMoreTherapist = document.createElement('div');
            showMoreSection.append(showMoreTherapist);

            let therapistAge = document.createElement('p');
            therapistAge.classList.add('card-text');
            therapistAge.textContent = 'Age: ' + therapistCardData.age;
            showMoreTherapist.append(therapistAge);

        }

    } catch (e) {
        console.log("error with data", e);
    }

    visit.push(response);
    saveCardToLocalStorage(visit);
}

////------------------------END ----------------function createRecord


// --------------------------------------------create card method POST
CREATE_CARD_btn.addEventListener('click', () => {
    fetch(URL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${TOKEN}`
        },
        body: JSON.stringify({
            title: `Visit to ${DOCTOR_btn.value}`,
            userName: USER_fullName.value,
            doctor: DOCTOR_btn.value,
            purpose: USER_purposeVisit.value,
            brief: USER_brief.value,
            urgency: URGENCY_btn.value,
            visitDate: USER_dentistDate.value,
            age: USER_cardioAge.value,
            pressure: USER_cardioPressure.value,
            massIndex: USER_cardioMass.value,
            diseases: USER_cardioDiseases.value,

        })
    })
        .then(response => response.json())
        .then(response => {
            createRecord(response);
            saveCardToLocalStorage(response);
        });

    MODAL_card.hide();
    EMPTY_section.style.display = 'none';
});


function saveCardToLocalStorage(response) {
    const existingCards = JSON.parse(localStorage.getItem('visit')) || [];

    if (Array.isArray(response)) {
        response.forEach((card) => {
            const isCardExists = existingCards.some((existingCard) => existingCard.id === card.id);
            if (!isCardExists) {
                existingCards.push(card);
            }
        });
    } else {
        const isCardExists = existingCards.some((existingCard) => existingCard.id === response.id);
        if (!isCardExists) {
            existingCards.push(response);
        }
    }
    localStorage.setItem('visit', JSON.stringify(existingCards));
}

function removeFromLocalStorage(cardId) {
    let existingCards = JSON.parse(localStorage.getItem('visit')) || [];
    existingCards = existingCards.filter((existingCard) => existingCard.id !== cardId);
    localStorage.setItem('visit', JSON.stringify(existingCards));
}

function loadCardsFromLocalStorage() {
    const visitData = localStorage.getItem('visit');
    if (visitData) {
        visit = JSON.parse(visitData);
        console.log(visit);
        if (Array.isArray(visit)) {
            visit.forEach((card) => {
                createRecord(card);
            });
        }
    }
}


///FILTER
FILTER_btn.addEventListener('click', () => {
    const searchText = FILTER.value.toLowerCase();

    visit.forEach(card => {

        console.log(card);

        const cardDoctor = card.doctor;
        const cardDescription = card.brief;

        console.log(card);

        if (cardDoctor.includes(searchText) || cardDescription.includes(searchText)) {
            card.style.display = 'block';
        } else {
            card.style.display = 'none';
        }
    });
});