// CLASSES

export class Visit {
    constructor(doctor, purpose, brief, urgency, userName) {
        this.doctor = doctor;
        this.purpose = purpose;
        this.brief = brief;
        this.urgency = urgency;
        this.userName = userName;
    }
}

export class VisitDentist extends Visit {
    constructor(doctor, purpose, brief, urgency, userName, lastVisit) {
        super(doctor, purpose, brief, urgency, userName);
        this.lastVisit = lastVisit;
    }
}

export class VisitCardiologist extends Visit {
    constructor(doctor, purpose, brief, urgency, userName, pressure, massIndex, diseases, age) {
        super(doctor, purpose, brief, urgency, userName);
        this.pressure = pressure;
        this.massIndex = massIndex;
        this.diseases = diseases;
        this.age = age;
    }
}

export class VisitTherapist extends Visit {
    constructor(doctor, purpose, brief, urgency, userName, age) {
        super(doctor, purpose, brief, urgency, userName);
        this.age = age;
    }
}