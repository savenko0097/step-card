export const URL = "https://ajax.test-danit.com/api/v2/cards";

// ====== SECTIONS ====== //
export const EMPTY_section = document.getElementById('empty-section'); //start section, before registration or initialization user
export const FORM_sectinon = document.getElementById('form-section'); // page, where user input email + password
export const CARD_section = document.getElementById('card-section'); //card section, after succsesfull registration
export const cardInfo = document.querySelector('.card-info');
export const RECORD_CARD_section = document.getElementById('post-cards');

// ====== BUTTONS ====== // 
export const LOGIN_btn = document.getElementById('login-btn'); // if user have no account show this page
export const REGISTRATION_btn = document.getElementById('registration'); // registration BTN, when user input email + password
export const LOG_OUT_btn = document.getElementById('logOut_btn');
export const MODAL_card = new bootstrap.Modal(document.getElementById('modal-create-card')); //modal to create a card
export const CLOSE_MODAL_btn = document.getElementById('close-modal');
export const CREATE_CARD_btn = document.getElementById('create-card'); //  btn to add card, when all inputs filled 
export const SAVE_CHANGES_CARD_btn = document.getElementById('edit-card');
export const ADD_CARD_btn = document.getElementById('add-card');
export const closeModal = document.querySelector('.close');
export const FILTER_btn = document.getElementById('filter-btn')

// ====== ELEMENTS ====== //
//LOGIN data
export const USER_email = document.getElementById('user-email');
export const USER_password = document.getElementById('user-password');
export const USER_repeat_pass = document.getElementById('repeat-user-password');
export const FILTER = document.getElementById('filter')

//CREATE CARD data
// user choose doctor, dropdown menu
export const DOCTOR_btn = document.getElementById('doctor-dropdown-menu');
export const URGENCY_btn = document.getElementById('urgency-dropdown-menu');
export const USER_purposeVisit = document.getElementById('purpose-of-visit');
export const USER_brief = document.getElementById('brief-textarea');
export const USER_urgancyReg = document.getElementById('urgancy-regular');
export const USER_urgancyPr = document.getElementById('urgancy-priority');
export const USER_urgancyUrg = document.getElementById('urgancy-urgent');
export const USER_fullName = document.getElementById('full-name-input');
// console.log(USER_fullName)

// when user choose doctor, add INPUTS for these doctor
export const DENTIS_input = document.getElementById('dentist-input');
export const CARDIOLOGIST_input = document.getElementById('cardiologist-input');
export const THERAPIST_input = document.getElementById('therapist-input');

//DENTIST
export const USER_dentistDate = document.getElementById('dentist-visit');

//CARDIOLOGIST
export const USER_cardioPressure = document.getElementById('cardiologist-pressure');
export const USER_cardioMass = document.getElementById('cardiologist-mass-index');
export const USER_cardioDiseases = document.getElementById('cardiologist-diseases');
export const USER_cardioAge = document.getElementById('cardiologist-age');

//THERAPIST
export const USER_therapistAge = document.getElementById('therapist-age');